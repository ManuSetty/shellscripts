#!/bin/sh
# 
# example script for submitting an array job to gridware. 
# #$ precedes parameters given to qsub
# I have included those I thought were most useful 
# you may not want to use all of them.
#
#  clear SGE defaults
#$ -clear
# Specifies the interpreting shell for the job.
#$ -S /bin/sh
# Execute the job from  the  current  working  directory.
#$ -cwd

# specifies the list of queues which may be used to execute this job. 
# 	if your jobs are long you may not want to monopolize the cluster.
#$ -q all.q

# Identifies the ability of a job to be rerun or not.  If
#          the  value  of  -r is 'y', rerun the job if the job was
#          aborted without leaving a consistent exit  state  (this
#          is  typically  the case if the node on which the job is
#          running crashes).  If -r is 'n', do not rerun  the  job
#          under any circumstances.
#$ -r n

# the name of the job, shows up in qstat
#$ -N bowtie2_align

# Defines the priority of the job relative to other jobs in the queue. 
# 	Priority is an integer in the range -1023 to 1024. If you want 
#	to let other jobs go in front of yours in the queue you can set
#	it lower. The default is 0. 
#$ -p 0

# Submits a so called Array Job, i.e. an array of identi-
#          cal  tasks being only differentiated by an index number
#          and being treated by Grid Engine almost like a series
#          of jobs. The option argument to -t specifies the number
#          of array job tasks and the index number which  will  be
#          associated  with  the  tasks. The index numbers will be
#          exported to the job tasks via the environment  variable
#          SGE_TASK_ID.   
#$ -t 1-23

# merge std error and std out into one file
#$ -j n

#STDOUT and STDERR of array job tasks  will  be  written
#          into different files with the locations
#          <jobname>.['e'|'o']<job_id>'.'<task_id>
#	This can get screwy if you are writing a lot to stdout.
#	But if you have a lot of jobs it will prevent you from
#	having a file for each one. You can give these any name.
#$ -o bowtie2_align.out
#$ -e bowtie2_align.err

# the variable $tasks holds the inputs for your script. myscriptfoo.pl would
# be the wrapper script that does your real work.  Each time 
# myscriptfoo.pl is called it gets one of the values from tasks 
# and is assigned a SGE_TASK_ID (defined above in -t) as ja-task-ID
# that you see in qstat.

##echo $input
files=$1
in_file=$(head -$SGE_TASK_ID $files | tail -1)
log_file=`echo ${in_file} | sed 's/fastq.*/log/g'`
bam_file=`echo ${in_file} | sed 's/fastq.*/bam/g'`
. ~/.bash_profile
bowtie2 -x /ifs/e63data/leslielab/local/share/igenomes/UCSC/mm9/Sequence/Bowtie2Index/genome -p 2 -U ${in_file} 2>>${log_file} | samtools view -bS -o ${bam_file} - 
samtools sort ${bam_file} ${bam_file}.sorted
mv ${bam_file}.sorted.bam ${bam_file}
samtools index ${bam_file}

