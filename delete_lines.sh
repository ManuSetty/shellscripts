#!/bin/sh

# Script to delete the first n lines of the files of the specified extension
# Manu N Setty
# 10/11/2009

if [ $# -ne 2 ]
then
    echo "Usage: delete_lines.sh <file type> <lines>"
    exit 1
fi

extension=$1
lines=$2

echo "Deleting first ${lines} lines from all files with extension ${extension} " `pwd`

for file in `ls *.${extension}`
do
	echo "Processing $file"
	cat $file | sed 1,${lines}d > tt
	mv tt $file
done