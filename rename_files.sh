#!/bin/sh

# Script to change files with one extension to another
# Manu N Setty
# 10/11/2009

if [ $# -ne 2 ]
then
    echo "Usage: rename_files.sh <from extension> <to extension>"
    exit 1
fi

from_extension=$1
to_extension=$2

echo "Renaming all files with extension ${from_extension} to extension ${to_extension} in" `pwd`

for file in `ls *.${from_extension}`
do
	echo "Renaming $file"
	new_name=`echo $file | sed "s/\.${from_extension}/\.${to_extension}/g"`
	mv $file ${new_name}
done